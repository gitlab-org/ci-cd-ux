<!-- Issue title: UX Showcase CI/CD - Month_Day -->

Hi team 👋! 

**We have a UX Showcasing coming up on _DATE_**. 

I'll be the host 🎤, which means your stage will need to present that day. Here's what you need to know:

* Each presentation is 15 minutes and you should come prepared with sufficient artifacts to tell a story. Use your best judgment to determine what will most effectively convey the story: a few slides, a process diagram, a journey map, a series of mockups, a prototype, etc.
* You do not need to create any special deliverable (such as slides) in order to present. You can simply walk everyone through a mock-up in Figma etc.
* Please make sure you leave some time for audience questions.

For more info, please refer to the [UX Showcase handbook page](https://about.gitlab.com/handbook/engineering/ux/ux-department-workflow/ux-showcase) and watch the [video recordings](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz) of previous showcases.

## Presenters

* `@designer`: `TOPIC`
* `@designer`: `TOPIC`
* `@designer`: `TOPIC`

## What I need from you

Please comment on this issue with the topic you'd like to present so I can fill out the agenda prior to the showcase.

Questions? Thoughts? Let's discuss it below 👇
