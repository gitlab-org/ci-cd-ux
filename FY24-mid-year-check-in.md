## Mid-Year Check In

### Achievements

_List your 3 most significant achievements since the last formal Talent Assessment, aligned with the job responsibilities and expectations of the role. This can include significant impact to the department or company, customer-impact, community-impacting, etc. Feedback can also be included in this section if applicable._

1. 
2. 
3. 

### Strengths


_In this area the goal is to determine 2-3 strengths in accordance with your role, job family, and job framework. Include examples when necessary to provide clarity or context._

1. 
2. 
3. 

### Opportunities

_In this area the goal is to determine 2-3 opportunity areas in accordance with your role, job family, and job framework. Include examples when necessary to provide clarity or context. Ensure there are actionable takeaways._

1. 
2. 
3. 

### Support Needed

_A place to outline any support needed from your manager for your ongoing success and development at GitLab._

1.
2.
3.

### Manager Feedback

_Managers will provide additional feedback or response to the self-assessment outlined above. Managers should be sure to reference the Achievements (3) and Roles & Responsibilities (Strengths and Opportunities) outlined by the team member above, and to any additional thoughts or feedback (2-3 points)._

### Next Steps

_A space for the team member and manager to document any relevant next steps stemming from this conversation. This could be a full Individual Growth Plan or a couple of key actions or development opportunities for the individual._
